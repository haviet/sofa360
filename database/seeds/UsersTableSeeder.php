<?php

use Illuminate\Database\Seeder;
use App\users;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'gender' => '1',
            'avatar' => '11',
            'status' => '1',

        ]);
    }
}
