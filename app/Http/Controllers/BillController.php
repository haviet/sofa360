<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\Bill_detail;
use App\Employees;
use App\Products;
use Cart;
use App\Customers;
class BillController extends Controller
{
    public function index()
    {
        $bill = Bill::all();
        $cus = Customers::all();
        $emp = Employees::all();
        return view('admin.bill.index', compact('bill', 'cus', 'emp'));
    }
    public function detailsBill($id) {
        $i = $id;
        $bill_detail = bill_detail::where('bill_id','=', $id)->get();
        $pro = Products::all();
        return view('admin.bill.details', compact('bill_detail', 'pro','i'));
    }

    public function addPro() {
        $cart = Cart::content();
        Cart::count();
        $count = $cart->count();
        $pro = Products::all();
        $emp = Employees::all();
        $total = Cart::subtotal();
        $date = date("Y-m-d");
    	return view('admin.bill.pro', compact('pro', 'count', 'emp', 'cart','total','date'));
    }

    public function addCart($id) {
        $pro = Products::find($id);
        Cart::add(['id' => $id, 
                    'name' => $pro->name, 
                    'qty' => 1, 
                    'price' => $pro->price,
                    'weight' => 1,
                    'options' => ['image' =>  $pro->image]]);
        return redirect()->back();
    }

    public function updatePro(Request $req) {
        $rowId = $req->get('rowId');
        $qty = $req->get('qty');
        $a = Cart::update($rowId, ['qty' => $qty]);
        return response([
            'rowId'=>$rowId,
            'status'=>true,
            'qty'=>$qty,
        ]);
    }


    public function store(Request $req) {
        $cart = Cart::content();

        $cus = new Customers;
        $cus->name = $req->name_cus;
        $cus->email = $req->email;
        $cus->gender = $req->gender;
        $cus->phone = $req->phone;
        $cus->address = $req->address;
        $cus->save();

        $bill = new Bill();
        $bill->customer_id = $cus->id;
        $bill->employee_id = $req->name_emp;
        $bill->date_order = date('Y-m-d H:i:s');
        $bill->total = $req->total;
        $bill->payment = $req->payment;
        $bill->save();

        foreach($cart as $item) {
            
            $bill_detail = new bill_detail;
           
            $bill_detail->bill_id = $bill->id;
            $bill_detail->product_id = $item->id;
            $bill_detail->quantity = $item->qty;
            $bill_detail->price = $item->price;
           
            $bill_detail->save();
            $pro = Products::where('id', '=' , $bill_detail->product_id)->first();
            $pro->quantity = $pro->quantity - $bill_detail->quantity;
            $pro->save();
        }
        
        Cart::destroy();

        return redirect('admin/bill')->with('success', 'Thêm hóa đơn thành công');
    }

    public function deletePro($rowId) {
        Cart::remove($rowId);
        return back();
    }

    public function deleteBill($id) {
        $bill = Bill::find($id);
        $bill->delete();
        return redirect()->back();
    }

    public function editBill($id) {
        $bill = Bill::find($id);
        $cus = Customers::all();
        $emp = Employees::all();
        return view('admin/bill/edit', compact('bill', 'cus', 'emp'));
    }

    public function updateBill(Request $req, $id) {
        $bill = Bill::find($id);
        $cus = Customers::find($bill->customer_id);
        $bill->employee_id = $req->name_emp;
        $bill->date_order = $req->date_order;
        $bill->payment = $req->payment;
        $bill->save();

        $cus->name = $req->name_cus;
        $cus->email = $req->email;
        $cus->gender = $req->gender;
        $cus->phone = $req->phone;
        $cus->address = $req->address;
        $cus->save();

        return redirect('admin/bill')->with('success', 'Chỉnh sửa hóa đơn thành công');
    }
}
