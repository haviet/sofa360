<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;

class CustomersController extends Controller
{
    public function index()
    {
    	$cus = Customers::all();

    	return view('admin.customer.index',compact('cus'));
    }

    public function edit($id)
    {
        $cus = Customers::find($id);
        return view('admin.customer.edit',compact('cus'));
    }  

    public function update(Request $req, $id)
    {
        $this->validate($req,[
            'phone' => 'max:11',
        ],[
            'phone.min' => 'Số điện thoại không được nhiều hơn 11 kí tự',
        ]);
        $cus = Customers::find($id);
        $cus->name = $req->name;
        $cus->gender = $req->gender;
        $cus->email = $req->email;
        $cus->address = $req->address;
        $cus->phone = $req->phone;
        $cus->save();
        return redirect('admin/customer')->with('success','Cập nhật thông tin thành công, hãy xem lại!');
    }
}
