<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Products;
use App\Http\Requests\StoreProductRequest;
use App\Category;

class productsController extends Controller
{
    public function index()
    {
        $products = Products::all();
        return view('admin.products.index',['products'=>$products]);
    }

    public function create()
    {
        $categories = Category::all(); 
    	return view('admin.products.addsv',compact('categories'));
    }

    public function store(StoreProductRequest $request)
    {   
        $file = $this->upload($request->file('file'),'admin_asset/img/imgsv/');
        $request->merge(['image' => $file]);
        $products = Products::create($request->all());
        return redirect('admin/products/createsv')->with('thongbao','Thêm sản phẩm thành công hãy kiểm tra lại');
    }

    public function detail(Request $request,$id)
    {
        $products = Products::find($id);
        $categories = Category::all();

        return view('admin.products.detail',compact('products','categories'));
    }

    public function on($id)
    {
        $student = Student::find($id);
        if ($student)
        {
            $student->status = 1;
            $student->save();
            return redirect('admin/student');
        }
        else
        {
            return redirect('admin/student');
        }    
    }

    public function off($id)
    {
        $student = Student::find($id);
        if ($student)
        {
            $student->status = 0;
            $student->save();
            return redirect('admin/student');
        }
        else
        {
            return redirect('admin/student');
        }    
    }

    public function destroy($id)
    {
        $products = Products::find($id);
        $products->delete();
        return redirect('admin/products');
    }

    public function getupdate($id)
    {
        $products = Products::find($id);
        return view('admin.products.update',['products' => $products]);
    }

    public function postupdate(Request $request, $id)
    {
        $products = Products::find($id);
        $this->validate($request,[
            'name' => 'min:2',
            'price' => 'integer',
        ],[
            'name.min' => 'Tên sản phẩm phải lớn hơn 2 kí tự',
            'price.integer' => 'Giá phải là số',
        ]);

        $products->name = $request->name;
        $products->description = $request->description;
        $products->price = $request->price;
            $products->size = $request->size;
                $products->color = $request->color;
                $products->quantity = $request->quantity;
       

        
        $products->save();
        return redirect('admin/products/getupdate/'.$id)->with('thongbao','Chỉnh sửa sản phẩm thành công hãy kiểm tra lại');
    }

    public function getedit($id){
        $products = Products::find($id);
        return view('admin.products.edit',['products'=>$products]);
    }

    public function postedit(Request $request,$id)
    {
        $request->validate(
            [
                'file' => 'required|min:1', 
            ],
            [
                'file.required' => 'Chưa chọn ảnh',
            ]);
        $products = Products::find($id);
        $file = $this->upload($request->file('file'), 'admin_asset/img/imgsv/');
        $request->merge(['image' => $file]);
        $products->image = $request->image;
        $products->save();
                
        return redirect('admin/products/editimage/'.$id)->with('thongbao','Chỉnh sửa sản phẩm thành công hãy kiểm tra lại');
    }

    public function editcategory(Request $request,$id)
    {
        $products = Products::find($id);
        $products->categories_id = $request->categories_id;
        $products->save();

        return redirect('admin/products/detail/'.$id)->with('thongbao','Chuyển loại sản phẩm thành công');
    }

    /**
     * @param $file
     * @return mixed
     */
    public function upload($file, $path)
    {
        $name = sha1(date('YmdHis') . str_random(30)) . str_random(2) . '.' . $file->getClientOriginalExtension();

        $file->move($path, $name);

        return $path . $name;
    }
}
