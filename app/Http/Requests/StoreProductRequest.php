<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'file' => 'required|file',
            'price' => 'required|integer',
            'size'=>'required',
            'color'=>'required',
            'categories_id'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên',
            'description.required' => 'Chưa nhập description',
            'file.required' => 'Chưa chọn ảnh',
            'price.required' => 'Chưa nhập price',
            'price.integer' => 'price phải là số',

            'size.required' => 'Chưa nhập kích thước',
            'color.required' => 'Chưa nhập color',

            'kichthuoc.required' => 'Chưa nhập kích thước',
            'mau.required' => 'Chưa nhập mau',
            'soluong.required' => 'Chưa nhập số lượng',
            'categories_id.required' => 'Chưa chọn thể loại',
        ];
    }
}
