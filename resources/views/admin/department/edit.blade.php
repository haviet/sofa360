@extends('admin.widget.index')
@section('content')    	
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="row">
			
			<form class="form-horizontal" role="form" action="{{route('update-dep', $dep->id)}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
				<div class="form-group text-center">
                    <label for="">Nhập tên phòng ban:</label> <br>
                    <input name="name" type="text" value="{{$dep->name}}" required placeholder="nhập tên tổ nhân viên" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
                    <label for="">Nhập mô tả:</label> <br>
                    <textarea name="desc" id="" cols="30" rows="10" required style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;">{{$dep->description}}</textarea> <br>
				</div>
				<button class="btn btn-danger" style="display: block; margin: auto;">Cập nhập</button>
                <a href="admin/department" style=" display: table; margin: 10px auto;" class="btn btn-success">Quay lại</a>

            </form>

		</div>
	</div>
</div>
@endsection