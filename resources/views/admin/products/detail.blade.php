@extends('admin.widget.index')
@section('content')     
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="user-profile">
			<div class="row">
				<div class="col-md-6">
					<div class="user-info-list panel panel-default">
						<a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/products" style="font-size: 20px;">Sản phẩm</a><span>/</span><span style="font-size: 18px; color: #4285f4;">Chi tiết</span>
						<div class="panel-heading panel-heading-divider">
							<div class="name">{{$products->name}}</div>
							<div class="image" style="width: 100%; display: block;"><img class="img" style="height: 250px !important;" src="{{$products->image}}" alt=""></div>
							<a href="admin/products/editimage/{{$products->id}}" class="btn btn-sm btn-primary btnedit">Sửa ảnh</a>
							<div class="panel-body">
								<table class="no-border no-strip skills">
									<tbody class="no-border-x no-border-y">
										<tr>
											<td class="icon"><span class="mdi mdi-cake"></span></td>
											<td class="item">Mô tả<span class="icon s7-gift"></span></td>
											<td>{{$products->description}}</td>
										</tr>
										<tr>
											<td class="icon"><span class="mdi mdi-cake"></span></td>
											<td class="item">Giá<span class="icon s7-gift"></span></td>
											<td>{{($products->price)}}</td>
										</tr>
										<tr>
											<td class="icon"><span class="mdi mdi-cake"></span></td>
											<td class="item">Kích thước<span class="icon s7-gift"></span></td>
											<td>{{($products->size)}}</td>
										</tr>
										<tr>
											<td class="icon"><span class="mdi mdi-cake"></span></td>
											<td class="item">Màu<span class="icon s7-gift"></span></td>
											<td>{{($products->color)}}</td>
										</tr>
										<tr>
											<td class="icon"><span class="mdi mdi-cake"></span></td>
											<td class="item">Số lượng<span class="icon s7-gift"></span></td>
											<td>{{($products->quantity)}}</td>
										</tr>
										<tr>
											<td class="icon"><span class="mdi mdi-cake"></span></td>
											<td class="item">Loại sản phẩm<span class="icon s7-gift"></span></td>
											<td>	@if(($products->categories_id==3)) Đôn to
													@elseif(($products->categories_id==2))	Ghế đơn
													@elseif(($products->categories_id==2))	Sofa bộ
													@elseif(($products->categories_id==2))	Sofa da
													@elseif(($products->categories_id==2))	Sofa da cao cấp
													@elseif(($products->categories_id==2))	Sofa da tân cổ điển
													@elseif(($products->categories_id==2))	Sofa da tiết kiệm diện tích
													@elseif(($products->categories_id==2))	Sofa nỉ
													@elseif(($products->categories_id==2))	Sofa văng da
													@else Sofa văng nỉ
												@endif</td>
										</tr>
										
											<td class="icon"><span class="mdi mdi-pin"></span></td>
											<td class="item">Thao tác<span class="icon s7-global"></span></td>
											<td><button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Chuyển loại sản phẩm</button></td>
										</tr>
									</tbody>
								</table>
								<form action="admin/products/editcategory/{{$products->id}}" method="POST" role="form" class="form-vertical">
									@csrf
									<div class="modal fade" id="myModal" role="dialog">
										<div class="modal-dialog modal-sm">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Chọn loại sản phẩm</h4>
												</div>
												<div class="modal-body">
													<div class="form-group text-center">
														<div class="col-md-4 col-sm-6 col-xs-12">
															<label for="categories_id">loại sản phẩm</label>
															<select class="form-group" name="categories_id">
																@foreach($categories as $ca)
																<option value="{{$ca->id}}">{{$ca->name}}</option>
																@endforeach
															</select>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button style="margin-top: 20px;" class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Bạn có muốn chuyển loại sản phẩm')">Chuyển danh mục</button>
													<button style="margin-top: 20px;" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>		
									</div>
								</form>
								<a href="admin/products" class="btn btn-success btn-lg">Quay lại</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endsection