@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/products" style="font-size: 20px;">Sản phẩm</a>
                    <h3>Danh sách sản phẩm</h3>
            </div>
        </div>
            <div class="text-center">
                @if(count($errors)>0)
                <div class="alert alert-danger text-center" style="width:500px;">
                    @foreach($errors->all() as $er)
                        {{$er}}<br>
                    @endforeach
                </div>
                @endif
                @if(session('thongbao'))
                    <div class="alert-success text-center" style="width:300px;margin-left: 300px;">
                {{session('thongbao')}}
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th style="width: 5%; text-align: center;">ID</th>
                                <th style="width: 15%;">Tên sản phẩm</th>
                                <th style="width: 25%;">Ảnh sản phẩm</th>
                                <th style="width: 25%;">Số lượng</th>
                                <th style="width: 20%;">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody >
                            @foreach($products as $pr)
                                <tr>
                                    <td style="width: 5%;" class="text-center"><span class="badge badge-success">{{$pr->id}}</span></td>
                                    <td style="width: 15%;" class="text-center"><span class="badge badge-success">{{$pr->name}}</span></td>
                                    <td style="width: 25%"><img class="img-responsive" style="width: 100px !important;" src="{{asset($pr->image)}}">
                                    </td>
                                    <td style="width: 25%" class="text-center"><span class="badge badge-success">{{$pr->quantity}}</span></td>
                                    <td style="width: 20%">
                                      
                                        <a class="btn btn-sm btn-warning" href='javascript:window.location.href ="admin/products/getupdate/{{$pr->id}}"' title="">Sửa</a>
                                        <a class="btn btn-sm btn-primary" href="javascript:window.location.href ='admin/products/detail/{{$pr->id}}'" title="">Xem</a>
                                        <a onclick="return confirm('Bạn có chắc chắn xoá không?')" class="btn btn-sm btn-danger" href="javascript:window.location.href ='admin/products/destroy/{{$pr->id}}'" title="">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
