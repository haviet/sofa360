<!DOCTYPE html>
<html>
<head>
    <base href="{{asset('admin_asset')}}">
    <title>Create nhân viên</title>
    <link href="admin_asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin_asset/css/font-awesome.css" rel="stylesheet">
    <style type="text/css">
        body{
            font-family: 'Roboto', Arial, sans-serif;
            font-size: 13px;
            line-height: 1.4;
            background: #eee;
        }
        .container-fluid{
            margin-right: auto;
            margin-left: auto;
        }
        .main-content{
            padding-top: 25px;
            padding-left: 25px;
            padding-right: 25px;
        }
        *{
            box-sizing: border-box;
        }
        .splash-container{
            max-width: 401px;
            margin: 50px auto;
        }
        div{
            display: block;
        }
        .splash-container .panel{
            margin-bottom: 30px;
        }
        .panel-border-color-primary{
            border-top-color: #4285f4;
        }
        .panel{
            background-color: #ffffff;
            box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.04);
            border-width: 0;
            border-radius: 3px;
        }
        .panel-default{
            border-color: #ddd;
        }
        .splash-container .panel-heading{
            text-align: center;
            margin-bottom: 20px;
            padding-top: 40px;
            padding-bottom: 0px;
        }
        .panel-heading{
            font-size: 18px;
            font-weight: 300px;
            padding-left: 0px;
            padding-right: 0px;
            padding-bottom: 10px;
            margin: 0px 20px;
            border-bottom-width: 0px;
            border-radius: 3px 3px 0 0;
        }
        .splash-description{
            text-align: center;
            display: block;
            line-height: 20px;
            font-size: 20px;
            color: #5a5a5a;
            margin-top: -20px;
            padding-bottom: 10px
        }
        .splash-container .panel .panel-body{
            padding: 20px 30px 15px;
        }
        .panel-body{
            border-radius: 0 0 3px 3px;
        }
        .form-control{
            border-width: 1px;
            border-top-color: #bdc0c7;
            box-shadow: none;
            padding: 10px 12px;
            font-size: 15px;
            transition: none;
            display: block;
            width: 100%;
            height: 48px;
            line-height: 1.5;
            background-color: #fff;
            border: 1px solid #d5d8de;
            border-radius: 2px;
        }
        .btn-xl{
            padding: 0px 12px;
            font-size: 15px;
            line-height: 43px;
            border-radius: 3px;
            font-weight: 500;
        }
        .btn-primary{
            background-color: #4285f4; 
        }
        .login-submit .btn{
            margin-top: 20px;
            width: 100%;
        }
        table {
            width: 100%;
        }
        th, td {
            padding: 6px 10px;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="container-fluid main-content">
        
        <div class="splash-container" style="max-width: 600px;">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading">
                    <span class="splash-description">Chỉnh sửa đơn hàng</span>
                </div>
                <div class="panel-body">
                    @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                    @endif

                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                    <form action="{{route('update-bill', $bill->id)}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        @foreach ($cus as $c)
                        @if ($bill->customer_id == $c->id)
                        <div class="form-group">
                            <label>Tên khách hàng</label>
                            <input value="{{$c->name}}" required type="text" placeholder="Nhập tên khách hàng" class="form-control" name="name_cus" autofocus>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input value="{{$c->email}}" required type="text" placeholder="Nhập email khách hàng" class="form-control" name="email" autofocus>
                        </div>
                        <div class="form-group">
                            <label>Giới tính</label>
                            <select name="gender" class="form-control">
                                @if ($c->gender == 1)
                                <option value="{{$c->gender}}" selected style="display:none">Nữ</option>
                                @else 
                                <option value="{{$c->gender}}" selected style="display:none">Nam</option>
                                @endif
                                <option value="1">Nữ</option>
                                <option value="0">Nam</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Số điện thoại khách hàng</label>
                            <input value="{{$c->phone}}" required type="number" placeholder="Nhập số điện thoại" class="form-control" name="phone" autofocus>
                        </div>
                        <div class="form-group">
                            <label>Địa chỉ khách hàng</label>
                            <input value="{{$c->address}}" required type="text" placeholder="Nhập địa chỉ" class="form-control" name="address" autofocus>
                        </div>
                        @endif
                        @endforeach
                        <div class="form-group">
                            <label>Tên nhân viên</label>
                            <select name="name_emp" id="" class="form-control">
                                @foreach ($emp as $e)
                                    @if ( $bill->employee_id == $e->id)
                                        <option value="{{$e->id}}" selected style="display:none">{{$e->name}}</option>
                                    @endif 
                                    <option value="{{$e->id}}">{{$e->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    
                        <div class="form-group">
                            <label>Ngày đặt hàng</label>
                            <input value="{{substr($bill->date_order, 0, -9) }}" type="date" class="form-control" name="date_order" autofocus>
                        </div>

                        <div class="form-group">
                            <label>Hình thức thanh toán</label>
                            <select name="payment" id="" class="form-control">
                                <option value="{{$bill->payment}}" selected style="display:none">{{$bill->payment}}</option>
                                <option>Thanh toán trực tuyến</option>
                                <option>Sau khi nhận hàng</option>
                            </select>
                        </div>

                        <div class="form-group login-submit">
                            <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Chỉnh sửa hóa đơn</button>
                        </div>

                        <div class="form-group">
                            <a href="admin/bill" style="display: block; margin: 0 auto;" class="btn btn-success btn-xl">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script>
    
    $('.changeQuantity').keypress(function (event) {
            event.preventDefault();
        });

        $('.total').on('keydown keypress', function (event) {
            event.preventDefault();
        });

    
    $(document).ready(function(){
        $('.update-cart').click(function(){
            var rowId =  $(this).attr('id-tr');
            var quantity = this.parentElement.querySelector('.changeQuantity').value;                
            var url='{!!route("update-pro")!!}';
            var data = {
            'rowId': rowId,
            'qty': quantity,
            };
            $.ajax({
                type: "get",
                url : url,    
                data: data,
                dataType: 'json',
                success: function(res) {
                    if(res.status){
                        location.reload();
                    }
                },
            });
        }) 
        
    })   

</script>