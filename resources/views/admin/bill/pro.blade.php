@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/student" style="font-size: 20px;">Thêm đơn hàng</a>
                    <h3>Chọn sản phẩm cần thêm vào đơn hàng</h3>
                    <p>Đã thêm {{$count}} sản phẩm</p>
            </div>
        </div>
            <div class="text-center">
                @if(session('success'))
                    <div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                        {{session('success')}}
                    </div>
                @endif 
            </div>
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                           
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%; text-align: center;">ID</th>
                                                <th style="width: 15%;">Tên sản phẩm</th>
                                                <th style="width: 15%;">Giá(VND)</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            @foreach($pro as $item)
                                                <tr>
                                                    <td class="text-center">{{$item->id}}</td>
                                                    <td class="text-center">{{$item->name}}</td>
                                                    <td>{{number_format($item->price)}}</td>
                                                    
                                                    <td style="width: 20%">
                                                        <a class="btn btn-sm btn-warning" title="" href="{{route('add-cart', $item->id)}}">Thêm</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                                        <div class="panel-heading">
                                            <span class="splash-description">Thêm mới đơn hàng</span>
                                        </div>
                                        <div class="panel-body">
                                            @if(session('thongbao'))
                                            <div class="alert alert-success">
                                                {{session('thongbao')}}
                                            </div>
                                            @endif
                        
                                            @foreach ($errors->all() as $error)
                                            <div>{{ $error }}</div>
                                            @endforeach
                                            @if ($count == 0)  
                                                <h6>Chọn sản phẩm trước khi tạo hóa đơn</h6>
                                            @else
                                            <form action="admin/bill/store" method="POST" enctype="multipart/form-data">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                <div class="form-group">
                                                    <label>Tên khách hàng</label>
                                                    <input required type="text" placeholder="Nhập tên khách hàng" class="form-control" name="name_cus" autofocus>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input required type="text" placeholder="Nhập email khách hàng" class="form-control" name="email" autofocus>
                                                </div>
                                                <div class="form-group">
                                                    <label>Giới tính</label>
                                                    <select name="gender" class="form-control">
                                                        <option value="1">Nữ</option>
                                                        <option value="0">Nam</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Số điện thoại khách hàng</label>
                                                    <input required type="number" placeholder="Nhập số điện thoại" class="form-control" name="phone" autofocus>
                                                </div>
                                                <div class="form-group">
                                                    <label>Địa chỉ khách hàng</label>
                                                    <input required type="text" placeholder="Nhập địa chỉ" class="form-control" name="address" autofocus>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tên nhân viên</label>
                                                    <select name="name_emp" id="" class="form-control">
                                                        @foreach ($emp as $e)
                                                        <option value="{{$e->id}}">{{$e->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Sản phẩm</label> <br>
                                                    <table border="1" style="width: 100%;">
                                                        <thead>
                                                            <th>Sản phẩm</th>
                                                            <th>Số lượng</th>
                                                            <th>Giá (VND)</th>
                                                            <th></th>
                                                            <th></th>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($cart as $item)
                                                             <tr>
                                                                <td>{{$item->name}}</td>
                                                                <td>
                                                                    
                                                                    @foreach ($pro as $p)
                                                                        
                                                                        @if ($p->id == $item->id)
                                                                            <input id-tr='{{$item->rowId}}' class="changeQuantity" 
                                                                            type="number" min="1" max='{{$p->quantity}}' value="{{$item->qty}}" 
                                                                            style="width: 100%;border: none;">
                                                                        @endif
                                                                    @endforeach
                                                                </td>
                                                                <td >{{number_format($item->qty*$item->price)}}
                                                                    </td>
                                                                <td>
                                                                    <a href="{{route('delete-pro', $item->rowId)}}" style="cursor:pointer">X</a>
                                                                </td>
                                                                <td id-tr='{{$item->rowId}}' style="cursor: pointer" class="update-cart">
                                                                    Update
                                                                </td>
                                                                
                                                             </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                        
                                                <div class="form-group">
                                                    <label>Tổng cộng: </label>
                                                    <input class="total" name="total" value="{{number_format(str_replace(',', '',$total))}} đồng" type="text" style="background:none;display: initial;border: none; outline: none;">
                                                </div>
                        
                                                <div class="form-group">
                                                    <label>Ngày đặt hàng</label>
                                                    <input value="{{$date}}" type="date" class="form-control" name="date_order" autofocus>
                                                </div>
                        
                                                <div class="form-group">
                                                    <label>Hình thức thanh toán</label>
                                                    <select name="payment" id="" class="form-control">
                                                        <option>Thanh toán trực tuyến</option>
                                                        <option>Sau khi nhận hàng</option>
                                                    </select>
                                                </div>
                        
                                                <div class="form-group login-submit">
                                                    <p>Vui lòng update giỏ hàng trước khi thêm</p>
                                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Thêm hóa đơn</button>
                                                </div>
                        
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<style>
    .dataTables_paginate {
        margin: 0px !important;
    }

    
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
    $('.changeQuantity').keypress(function (event) {
            event.preventDefault();
        });

        $('.total').on('keydown keypress', function (event) {
            event.preventDefault();
        });

    
    $(document).ready(function(){
        $('.update-cart').click(function(){
            var rowId =  $(this).attr('id-tr');
            var quantity = this.parentElement.querySelector('.changeQuantity').value;                
            var url='{!!route("update-pro")!!}';
            var data = {
            'rowId': rowId,
            'qty': quantity,
            };
            $.ajax({
                type: "get",
                url : url,    
                data: data,
                dataType: 'json',
                success: function(res) {
                    if(res.status){
                        location.reload();
                    }
                },
            });
        }) 
        
    })   
</script>
@endsection
