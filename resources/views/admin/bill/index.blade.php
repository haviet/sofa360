@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/bill" style="font-size: 20px;">Hóa đơn</a>
                    <h3>Danh sách hóa đơn</h3>
            </div>
        </div>
            <div class="text-center">
               
                @if(session('success'))
                    <div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                        {{session('success')}}
                    </div>
                @endif 
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th style="width: 5%; text-align: center;">ID</th>
                                <th style="width: 15%; text-align: center;">Khách hàng</th>
                                <th style="width: 15%; text-align: center;">Nhân viên</th>
                                <th>Ngày đặt hàng</th>
                                <th>Tổng cộng(VND)</th>
                                <th>Hình thức thanh toán</th>
                                <th>Ghi chú</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody >
                            @foreach($bill as $item)
                                <tr>
                                    <td class="text-center">{{$item->id}}</td>
                                    <td class="text-center">
                                        @foreach ($cus as $c)
                                            @if ($item->customer_id ==  $c->id)
                                                {{$c->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        @foreach ($emp as $e)
                                            @if ($item->employee_id ==  $e->id)
                                            {{$e->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$item->date_order}}</td>
                                    <td>{{$item->total}}</td>
                                    <td>{{$item->payment}}</td>
                                    <th>{{$item->note}}</th>
                                    <td style="width: 20%">
                                        <a href="{{route('edit-bill', $item->id)}}" class="btn btn-sm btn-warning" title="">Sửa</a> 

                                        <a onclick="return confirm('Bạn có muốn xoá')" href="{{route('delete-bill', $item->id)}}" class="btn btn-sm btn-danger"  title="">Xoá</a>
                                        <a href="{{route('details-bill', $item->id)}}" class="btn btn-sm btn-warning" title="">Chi tiết</a> 

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
