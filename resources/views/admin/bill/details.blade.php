@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/student" style="font-size: 20px;">Chi tiết hóa đơn</a>
                    <h3>Danh sách chi tiết hóa đơn thuộc hóa đơn {{$i}}</h3>
            </div>
        </div>
            <div class="text-center">
                @if(session('success'))
                    <div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                        {{session('success')}}
                    </div>
                @endif 
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Mã bill</th>
                                <th>Tên sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Giá sản phẩm</th>
                                <th>Tổng giá</th>
                            </tr>
                        </thead>
                        <tbody >
                            @foreach($bill_detail as $item)
                                <tr>
                                    <td class="text-center">{{$item->id}}</td>
                                    <td class="text-center">{{$i}}</td>
                                    <td>
                                        @foreach ($pro as $p)
                                            @if ($item->product_id ==  $p->id)
                                            {{$p->name}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{$item->price}}</td>
                                    <td>{{$item->quantity * $item->price}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
