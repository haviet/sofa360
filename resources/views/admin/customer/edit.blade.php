@extends('admin.widget.index')
@section('content')    	
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="row">
				@if(session('success'))
                        <div class="alert alert-success">
                        {{session('success')}}
                        </div>
				@endif
				@foreach ($errors->all() as $error)
				<div style="color: red, margin-left: 50px">{{ $error }}</div>
				@endforeach
			<form class="form-horizontal" role="form" action="admin/customer/update/{{$cus->id}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
				<div class="form-group text-center">
                    <label for=""  style="width: 113px;">Nhập tên:</label> 
                    <input name="name" type="text" value="{{$cus->name}}" required placeholder="nhập tên khách hàng" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Chọn giới tính:</label> 
					<select name="gender" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;">
						<option value="0" {{ $cus->gender == 0 ? 'selected = "selected"' : '' }}>Nam</option>
						<option value="1" {{ $cus->gender == 1 ? 'selected = "selected"' : '' }}>Nữ</option>
					</select> <br><br>
					
					<label style="width: 113px;">Nhập địa chỉ email:</label> 
					<input name="email" type="text" value="{{$cus->email}}" required placeholder="nhập địa chỉ email" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>

					<label style="width: 113px;">Nhập địa chỉ:</label> 
					<input name="address" type="text" value="{{$cus->address}}" required placeholder="nhập địa chỉ khách hàng" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>

					<label style="width: 113px;">Nhập số điện thoại:</label> 
					<input name="phone" type="number" value="{{$cus->phone}}" required placeholder="nhập số điện thoại khách hàng" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
				</div>
				<button class="btn btn-danger" style="display: block; margin: auto;">Cập nhập</button>
                <a href="admin/customer" style=" display: table; margin: 10px auto;" class="btn btn-success">Quay lại</a>

            </form>
			
		</div>
	</div>
</div>
@endsection